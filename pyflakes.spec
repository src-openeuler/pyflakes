%global _empty_manifest_terminate_build 0
%global common_desc \
	Pyflakes A simple program which checks Python source files for errors.Pyflakes
Name:           python-pyflakes
Version:        3.2.0
Release:        1
Summary:        passive checker of Python programs
License:        MIT
URL:            https://github.com/PyCQA/pyflakes
Source0:        https://files.pythonhosted.org/packages/source/p/pyflakes/pyflakes-%{version}.tar.gz

BuildArch:      noarch
%description
%{common_desc}

%package -n python3-pyflakes
Summary:        passive checker of Python programs
Provides:       python-pyflakes
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
%description -n python3-pyflakes
%{common_desc}

%package help
Summary:        passive checker of Python programs
Provides:       python3-pyflakes-doc
%description help
%{common_desc}

%prep
%autosetup -n pyflakes-%{version} -p1

%build
%py3_build


%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%check
%{__python3} setup.py test

%files -n python3-pyflakes -f filelist.lst
%dir %{python3_sitelib}/*


%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Feb 22 2024 xu_ping <707078654@qq.com> - 3.2.0-1
- Upgrade package to version 3.2.0

* Thu Aug 3 2023 Dongxing Wang <dxwangk@isoftstone.com> - 3.1.0-1
- Upgrade package to version 3.1.0

* Sat Jul 22 2023 xu_ping <707078654@qq.com> - 3.0.1-2
- fix test failure due to python3.11

* Fri Apr 14 2023 wulei <wu_lei@hoperun.com> - 3.0.1-1
- Upgrade package to version 3.0.1

* Wed Mar 30 2022 xu_ping <xuping33@huawei.com> - 2.3.1-2
- Fix syntax error offsets for python 3.10

* Mon Jul 19 2021 OpenStack_SIG <openstack@openeuler.org> - 2.3.1-1
- update to 2.3.1

* Mon Nov 24 2020 zhangjiapeng <zhangjiapeng9@huawei.com> - 2.2.0-1
- update to 2.2.0

* Wed Oct 21 2020 wutao <wutao61@huawei.com> - 2.1.0-2
- delete python2 modules

* Tue Jun 23 2020 yanan li <liyanan032@huawei.com> - 2.1.0-1
- Package update

* Fri Nov 15 2019 zhujunhao <zhujunhao5@huawei.com> - 2.0.0-8
- package init
